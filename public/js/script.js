$(document).ready(function(){

	$('.tombolTambahData').on('click', function(){

		$('#judulModal').html('Tambah Data Fanfiction')
		$('.modal-footer button[type=submit]').html('Tambah Data')

	})

	$('.tampilModalUbah').on('click', function(){

		$('#judulModal').html('Ubah Data Fanfiction')
		$('.modal-footer button[type=submit]').html('Ubah Data')
		$('.modal-body form').attr('action', 'http://localhost/phpmvc/public/fanfiction/ubah')

		const id = $(this).data('id')

		$.ajax({
			url: 'http://localhost/phpmvc/public/fanfiction/getubah',
			data: {id : id},
			method: 'post',
			dataType: 'json',
			success: function(data){
				$('#title').val(data.title)
				$('#author').val(data.author)
				$('#couple').val(data.couple)
				$('#genre').val(data.genre)
				$('#id').val(data.id)
			}
		})

	})	



})