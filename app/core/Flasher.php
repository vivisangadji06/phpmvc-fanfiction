<?php 

class Flasher {
	
	public static function setFlash($pesan, $aksi, $tipe) {
		$_SESSION['flash'] = [
			"pesan" => $pesan,
			"aksi" => $aksi,
			"tipe" => $tipe
		];
	}


	// untuk menampilkan pesan flash-nya
	public static function flash(){

        if( isset($_SESSION['flash']) ) {
            echo '<div class="alert alert-' . $_SESSION['flash']['tipe'] .' alert-dismissible fade show" role="alert"> 
            		Data Fanfiction 
                    <strong>'. $_SESSION['flash']['pesan'] .'</strong> '. $_SESSION['flash']['aksi'] .'
                  </div>';
            unset($_SESSION['flash']);
        }

	}

}






 ?>