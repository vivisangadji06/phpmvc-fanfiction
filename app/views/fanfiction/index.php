<div class="container mt-4">
	
	<div class="row">
		<div class="col-lg-6">
			<?php Flasher::flash(); ?>
		</div>
	</div>

	<div class="row mb-3">
		<div class="col-lg-6">
			<!-- Button trigger modal -->
			<button type="button" class="btn btn-primary tombolTambahData" data-bs-toggle="modal" data-bs-target="#formModal">
			  Tambah Data
			</button>
		</div>
	</div>

	<div class="row mb-3">
		<div class="col-lg-6">
			<form action="<?= BASE_URL; ?>/fanfiction/cari" method="post">
				<div class="input-group">
				  <input type="text" class="form-control" placeholder="Ketikan pencarian" name="keyword" id="keyword" aria-label="Recipient's username" aria-describedby="button-addon2" autocomplete="off">
				  <button class="btn btn-primary" type="submit" id="tombolCari">Cari</button>
				</div>
			</form>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-6">
			<h3>Daftar Fanfiction</h3>
				<ul class="list-group">
				  <?php foreach ($data['fanfic'] as $story) : ?>	
				    <li class="list-group-item">
				    	<?= $story['title']; ?>
				    	<a href="<?= BASE_URL; ?>/fanfiction/hapus/<?= $story['id'] ?>" class="badge bg-danger float-end mx-1" style="text-decoration: none;" onclick="return confirm('Yakin ingin menghapus data?');">hapus
				    	</a>
				    	<a href="<?= BASE_URL; ?>/fanfiction/ubah/<?= $story['id'] ?>" class="badge bg-success float-end mx-1 tampilModalUbah" style="text-decoration: none;" data-bs-toggle="modal" data-bs-target="#formModal" data-id="<?= $story['id']?>">edit
				    	</a>
				    	<a href="<?= BASE_URL; ?>/fanfiction/detail/<?= $story['id'] ?>" class="badge bg-primary float-end" style="text-decoration: none;">detail
				    	</a>
				    </li>
				  <?php endforeach; ?>
				</ul>
		</div>
	</div>
</div>


<!-- Modal -->
<div class="modal fade" id="formModal" tabindex="-1" aria-labelledby="judulModal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="judulModal">Tambah Data Fanfiction</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form action="<?= BASE_URL; ?>/fanfiction/tambah" method="post">
        	<input type="hidden" name="id" id="id">
	    	 <div class="mb-3">
			    <label for="title" class="form-label">Title</label>
			    <input type="text" class="form-control" id="title" name="title">
				 </div>
				 <div class="mb-3">
				    <label for="author" class="form-label">Author</label>
				    <input type="text" class="form-control" id="author" name="author">
				 </div>
				 <div class="mb-3">
				    <label for="couple" class="form-label">Couple</label>
				    <input type="text" class="form-control" id="couple" name="couple">
				 </div>
				 <div class="mb-3">
				    <label for="genre" class="form-label">Genre</label>
				    <input type="text" class="form-control" id="genre" name="genre">
				 </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Tambah Data</button>
        </form>
      </div>
    </div>
  </div>
</div>