<div class="container mt-4">
	
	<div class="card" style="width: 18rem;">
	  <div class="card-body">
	    <h5 class="card-title"><?= $data['fanfic']['title'] ?></h5>
	    <h6 class="card-subtitle mb-2 text-muted"><?= $data['fanfic']['author'] ?></h6>
	    <p class="card-text"><?= $data['fanfic']['couple'] ?></p>
	    <p class="card-text"><?= $data['fanfic']['genre'] ?></p>
	    <a href="<?= BASE_URL; ?>/fanfiction" class="btn btn-primary">Kembali</a>
	  </div>
	</div>




</div>