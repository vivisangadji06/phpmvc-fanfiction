<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Halaman <?= $data['judul']; ?></title>
	<link rel="stylesheet" type="text/css" href="<?= BASE_URL; ?>/css/bootstrap.min.css">
</head>
<body>

	<!-- Navbar -->
	<nav class="navbar navbar-expand-lg navbar-dark bg-warning">
	  <div class="container">
	    <a class="navbar-brand" href="<?= BASE_URL; ?>">Fanfiction MVC</a>
	    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
	      <span class="navbar-toggler-icon"></span>
	    </button>
	    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
	      <div class="navbar-nav">
	        <a class="nav-link active" aria-current="page" href="<?= BASE_URL; ?>">Home</a>
	        <a class="nav-link" href="<?= BASE_URL; ?>/fanfiction">Story</a>
	        <a class="nav-link" href="<?= BASE_URL; ?>/about">About</a>
	      </div>
	    </div>
	  </div>
	</nav>
	<!-- /.Navbar -->