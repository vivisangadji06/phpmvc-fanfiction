<div class="container bg-light p-4">
	<h1>Selamat Datang di Halaman About</h1>
	<p>Halo, nama saya <?= $data['nama'] ?>. Umur saya <?= $data['umur'] ?> tahun, saya adalah seorang <?= $data['pekerjaan'] ?></p>
  	<img src="<?= BASE_URL; ?>/image/profile.png" width="200" class="rounded-circle shadow">
  	<hr class="my-4">
  	<a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a>
</div>

