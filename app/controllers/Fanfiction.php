<?php 

class Fanfiction extends Controller {

	public function index(){

		$data['judul'] = 'Daftar Fanfiction';
		$data['fanfic'] = $this->model('Fanfiction_model')->getAllFanfic();
		$this->view('templates/header', $data);
		$this->view('fanfiction/index', $data);
		$this->view('templates/footer');


	}

	public function detail($id){

		$data['judul'] = 'Detail Story';
		$data['fanfic'] = $this->model('Fanfiction_model')->getFanfictionById($id);
		$this->view('templates/header', $data);
		$this->view('fanfiction/detail', $data);
		$this->view('templates/footer');


	}

	public function tambah(){

		if ($this->model('Fanfiction_model')->tambahData($_POST) > 0) {
			Flasher::setFlash('berhasil', 'ditambahkan', 'success');
			header("Location: ".BASE_URL."/fanfiction/");
			exit;
		} else {
			Flasher::setFlash('gagal', 'ditambahkan', 'danger');
			header("Location: ".BASE_URL."/fanfiction/");
			exit;
		}
	}

	public function hapus($id){

		if ($this->model('Fanfiction_model')->hapusData($id) > 0) {
			Flasher::setFlash('berhasil', 'dihapus', 'success');
			header("Location: ".BASE_URL."/fanfiction/");
			exit;
		} else {
			Flasher::setFlash('gagal', 'dihapus', 'danger');
			header("Location: ".BASE_URL."/fanfiction/");
			exit;
		}

	}

	public function getubah(){

		echo json_encode($this->model('Fanfiction_model')->getFanfictionById($_POST['id']));

	}

	public function ubah(){

		if ($this->model('Fanfiction_model')->ubahData($_POST) > 0) {
			Flasher::setFlash('berhasil', 'diubah', 'success');
			header("Location: ".BASE_URL."/fanfiction/");
			exit;
		} else {
			Flasher::setFlash('gagal', 'diubah', 'danger');
			header("Location: ".BASE_URL."/fanfiction/");
			exit;
		}
	}

	public function cari(){
		$data['judul'] = 'Daftar Fanfiction';
		$data['fanfic'] = $this->model('Fanfiction_model')->cariDataFanfic();
		$this->view('templates/header', $data);
		$this->view('fanfiction/index', $data);
		$this->view('templates/footer');

	}





}





 ?>