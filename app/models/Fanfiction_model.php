<?php 

class Fanfiction_model {
	private $table = 'fanfic';
	private $db;

	public function __construct(){
		$this->db = new Database;
	}

	public function getAllFanfic() {

		$this->db->query('SELECT * FROM '.$this->table);
		return $this->db->resultSet();

	}

	public function getFanfictionById($id) {

		$this->db->query('SELECT * FROM fanfic WHERE id=:id');
		$this->db->bind('id', $id);
		return $this->db->single();

	}

	public function tambahData($data){
		$query = "INSERT INTO fanfic VALUES ('', :title, :author, :couple, :genre)";
		$this->db->query($query);
		$this->db->bind('title', $data['title']);
		$this->db->bind('author', $data['author']);
		$this->db->bind('couple', $data['couple']);
		$this->db->bind('genre', $data['genre']);

		$this->db->execute();

		return $this->db->rowCount();
	}

	public function hapusData($id) {
		
		$query = "DELETE FROM fanfic WHERE id = :id";
		$this->db->query($query);
		$this->db->bind('id', $id);

		$this->db->execute();

		return $this->db->rowCount();
	}

	public function ubahData($data){

		$query = "UPDATE fanfic SET title = :title, author = :author, couple = :couple, genre = :genre WHERE id = :id";
		$this->db->query($query);
		$this->db->bind('title', $data['title']);
		$this->db->bind('author', $data['author']);
		$this->db->bind('couple', $data['couple']);
		$this->db->bind('genre', $data['genre']);
		$this->db->bind('id', $data['id']);

		$this->db->execute();

		return $this->db->rowCount();
	}

	public function cariDataFanfic(){

		$keyword = $_POST['keyword'];
		$query = "SELECT * FROM fanfic WHERE title LIKE :keyword";
		$this->db->query($query);
		$this->db->bind('keyword', "%$keyword%");

		return $this->db->resultSet();

	}


}

